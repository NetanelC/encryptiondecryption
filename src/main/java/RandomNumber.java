import java.util.Random;

public class RandomNumber {
    public Integer  randomInt()  {

        java.util.Random rand = new java.util.Random();

        // Generate random integers in range 0 to 999
        int rand_int = rand.nextInt(1000);
        return rand_int ;
    }
}
