import java.io.IOException;

public class Encrypted {
    public static void  handleEncryptData() throws IOException {
        System.out.println("You choose to Encrypt ");
        FileHandle localFileHandle = new FileHandle() ;
        String localPath = localFileHandle.FilePath("What is the file name you want encrypt ?") ;
        boolean fileExists = localFileHandle.FileExists(localPath);
        if (!fileExists) {
            System.out.println("The File doesn't exists ");
            System.exit(1) ;
        }
        String fileData = localFileHandle.FileRead(localPath);

        if (fileData.length() > 0) {

            //split the string into array
            char[] splitChar = null;
            StringHandle locLStringHandle = new StringHandle();
            //do the split
            splitChar = locLStringHandle.splitToaArray(fileData);

            //get the path from file name
            String filepath = localFileHandle.GetPathOnly(localPath);

            //create the random key
            RandomNumber localRandom = new RandomNumber();
            int key = localRandom.randomInt();

            //build the encryptedFile name
            String encryptedFile = keyFileName(localPath,filepath);
            localFileHandle.FileWrite(encryptedFile, Integer.toString(key));
            System.out.println("The key file name is: " + encryptedFile);

            //encrypt the data
            String encryptData = encryptData(splitChar, key);

            //rename the  encrypt file name
            String newFileName = localFileHandle.RenameFile(localPath, "_encrypted");

            //save the  encrypt file
            localFileHandle.FileWrite(newFileName, encryptData);
            System.out.println("The encrypt file name is:" + newFileName);

        }

    }

        public static String  encryptData(char dataToEncrypt[], int key) {
            String encryptString = "";
            String combinedData = "";
            for (int i = 0; i < dataToEncrypt.length; i++) {
                combinedData = String.valueOf(dataToEncrypt[i]) + String.valueOf(key);
                encryptString = encryptString + combinedData;
            }
            return encryptString;
        }

            public static String  encryptFileName(String localPath, String filepath){
                FileHandle localFileHandle = new FileHandle() ;
                String fileExtension = localFileHandle.GetFileExtension(localPath);
                String originalFileName = localFileHandle.GetFileName(localPath);
                String encryptedFileName = filepath + originalFileName + "key" + fileExtension;
                return encryptedFileName ;
            }

    public static String  keyFileName(String localPath, String filepath){
        FileHandle localFileHandle = new FileHandle() ;
        String fileExtension = localFileHandle.GetFileExtension(localPath);
        String encryptedFileName = filepath + "key" + fileExtension;
        return encryptedFileName ;
    }
    }
