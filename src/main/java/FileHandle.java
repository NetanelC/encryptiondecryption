import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner ;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileHandle {

    public String FilePath(String question)  {
        Scanner in = new Scanner(System.in);
        System.out.println(question);
        String inputUser = in.nextLine();
        File file = new File(inputUser);
        return inputUser;
    }

    public boolean FileExists(String FileName){
        File localFile = new File(FileName);
        boolean exists = localFile.exists();
        return exists ;
    }

    public String  FileRead(String FileName) {
        String fiesta = "";
        try {
            File myObj = new File(FileName);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                fiesta = myReader.nextLine();
                //System.out.println(fiesta);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return  fiesta ;
    }


    public static void FileWrite(String fileName, String stringToWrite) {
        try {
            FileWriter myWriter = new FileWriter(fileName);
            myWriter.write(stringToWrite);
            myWriter.close();
            //System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred when trying to write to : " + fileName);
            e.printStackTrace();
        }
    }


    public static String RenameFile(String fileName, String whatToAdd) {

            String fileExtension = GetFileExtension(fileName);
            int pos = fileName.lastIndexOf(".");
            String newFileName = GetPathOnly(fileName);
            String originalFileName = GetFileName(fileName);
            newFileName = newFileName + originalFileName + whatToAdd + fileExtension;
            return newFileName;
        }

    public static String GetPathOnly (String fileName) {
        int pos = fileName.lastIndexOf("\\");
        String obsoletePath = fileName.substring(0, pos + 1 );
        return obsoletePath;
    }

    public static String GetFileExtension(String fileName) {
        int dotPos = fileName.lastIndexOf(".");
        String fileExtension = fileName.substring(dotPos);
        return fileExtension ;
    }

    public static String GetFileName(String fileName) {
        int slashPos = fileName.lastIndexOf("\\") + 1;
        int dotPos = fileName.lastIndexOf(".");
        String newFileName = fileName.substring(slashPos,dotPos);
        return newFileName ;
    }

    public String FileReplaceData (String fileData, String dataToReplace, String replaceWith) {
        if (fileData.length() > 0)
        {
            String dataToReturn = fileData.replace(dataToReplace, replaceWith);
            return dataToReturn;
        }
        else {
            return fileData ;
        }

    }

}
